$(document).ready(function() {
  const getCookies = Cookies.get("name");

  if (getCookies === "accepted") {
    $(".M27-Cookies").removeClass("M27-Cookies__open");
  } else {
    $(".M27-Cookies").addClass("M27-Cookies__open");
  }

  $(".M27-Cookies__Button").click(function() {
    Cookies.set(
      "name",
      "accepted",
      { expires: 365, path: "/" },
      { secure: true }
    );
    $(".M27-Cookies").removeClass("M27-Cookies__open");
  });
});
