(function($, window, undefined) {
  $.fn.validate = function() {
    var $form = $(this),
      $submit = $form.find("#newsletter-submit"),
      $validateOn = $form.data("validation-event-newsletter"),
      $checkRquired = function(element) {
        if (element.val().trim().length) return true;
        else return false;
      },
      $setState = function(element, state) {
        if (!state)
          element
            .prop("placeholder", element.attr("data-validation-msg-newsletter"))
            .parent()
            .removeClass("success")
            .addClass("error");
        else
          element
            .prop("placeholder", element.attr("data-validation-msg-newsletter"))
            .parent()
            .removeClass("error")
            .addClass("success");
      },
      $checkPattern = function(element) {
        var re = new RegExp(element.attr("data-validation-pattern-newsletter"));
        return re.test(element.val());
      },
      $checkType = function(element) {
        switch (element.attr("type")) {
          case "number":
            var re = /[0-9]+/;
            return re.test(element.val());
          default:
            return element.val().trim().length > 0;
        }
      },
      $validate = function(element) {
        var $state = false;
        if (element.attr("data-validation-newsletter") == "required")
          $state = $checkRquired(element);
        if (element.attr("type")) $state = $checkType(element);
        if (element.attr("data-validation-pattern-newsletter"))
          $state = $checkPattern(element);
        return $state;
      };
    var $valid = false;
    $form.on($validateOn, function() {
      $(
        $form
          .find("input[data-validation-newsletter]")
          .get()
          .reverse()
      ).each(function() {
        if (this.value == "") {
          $(this).focus();
        }
      });
      if ($form.find(".error").length) return false;
    });

    $form.find("input[data-validation-newsletter]").each(function() {
      var $this = $(this),
        $events = $this.attr("data-validation-event-newsletter");
      if ($events) {
        $events = $this.attr("data-validation-event-newsletter").split(" ");
      } else {
        $events = ["keyup", "blur"];
      }

      $this.on({
        blur: function() {
          if ($events.includes("blur")) $valid = $validate($this);
          $setState($this, $valid);
        },
        change: function() {
          if ($events.includes("change")) $valid = $validate($this);
          $setState($this, $valid);
        },
        keyup: function() {
          if ($events.includes("keyup")) $valid = $validate($this);
          $setState($this, $valid);
        }
      });
    });
    $submit.attr("disabled", false);
  };
  $('form[data-validation-newsletter="true"]').validate();
})(jQuery);
