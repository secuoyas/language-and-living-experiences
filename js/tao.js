/*!
 * Menu nav desplegar
 */

$(".M02-Header__button_menu").click(function(e) {
  $(".M05-Menu").toggleClass("M05-Menu__open");
  $("body").toggleClass("modal-open");
  event.preventDefault();
});
$(".M05-Menu__button_menu").click(function() {
  $(".M05-Menu").toggleClass("M05-Menu__open");
  $("body").toggleClass("modal-open");
  event.preventDefault();
});

/*!
 * Menu boton contacto
 */

$(".ButAbrirContacto").click(function(e) {
  $(".M10-Modal-Contacto").toggleClass("M10-Modal-Contacto__open");
  $("body").toggleClass("modal-open");
  event.preventDefault();
});
$(".M10-Modal-Contacto-button_menu").click(function() {
  $(".M10-Modal-Contacto").toggleClass("M10-Modal-Contacto__open");
  $("body").toggleClass("modal-open");
  event.preventDefault();
});

/*!
 * FAQ
 */

$(".M20-Faqs-Card-Link").click(function(e) {
  $(this)
    .children(".M20-Faqs-Card-Icon")
    .toggleClass("icon-plus icon-minus");
  event.preventDefault();
});

/*!
 * Menu scroll header, cuando llega a un punto de scroll cambia clases
 */

$(window).scroll(function() {
  if ($(window).scrollTop() >= 300) {
    $(".M02-Header").addClass("M02-Header--Scroll");
  } else {
    $(".M02-Header").removeClass("M02-Header--Scroll");
  }
});

/* scrollTop() >= 240
   Should be equal the the height of the header
 */

/*!
 * Galeria fotos
 */

$(".GalleryJump").each(function(index, element) {
  $(element).click(function(event) {
    $(".M26-The-Gallery").trigger("to.owl.carousel", index);
    $(".M26LightBoxGallery").toggleClass("M26__open");
    $("body").toggleClass("modal-open");
    event.preventDefault();
  });
});

$(".M26-Button-Close").click(function() {
  $(".M26LightBoxGallery").toggleClass("M26__open");
  $("body").toggleClass("modal-open");
  event.preventDefault();
});
